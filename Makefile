.PHONY: host

all: host

wix.json:
	cp wix.json.template wix.json
	while grep '%UUID%' wix.json; do sed -i -E -e "s/%UUID%/$${uuidgen}/" wix.json; done

host:
	./build.sh
	echo "Run build.bat on Windows to build MSI."

