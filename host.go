/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	rotatelogs "github.com/lestrrat/go-file-rotatelogs"
	"github.com/lhside/chrome-go"
	"log"
	"os"
	"path/filepath"
	"time"
)

const VERSION = ""

var RunInCLI bool
var DebugLogs []string
var Logging bool
var Debug bool

func LogForInfo(message string) {
	DebugLogs = append(DebugLogs, message)
	if Logging {
		if !RunInCLI {
			fmt.Fprintf(os.Stderr, "[info] "+message+"\n")
		}
		log.Print(message + "\r\n")
	}
}

func LogForDebug(message string) {
	DebugLogs = append(DebugLogs, message)
	if Logging && Debug {
		if !RunInCLI {
			fmt.Fprintf(os.Stderr, "[debug] "+message+"\n")
		}
		log.Print(message + "\r\n")
	}
}

type Request struct {
	Logging          bool   `json:"logging"`
	Debug            bool   `json:"debug"`
	LogRotationCount int    `json:"logRotationCount"`
	LogRotationTime  int    `json:"logRotationTime"`
	Command          string `json:"command"`
}

func main() {
	shouldReportVersion := flag.Bool("v", false, "version information")
	commandLineCommand := flag.String("c", "", "command to run")
	commandLineDebug := flag.Bool("d", false, "debug mode")
	flag.Parse()

	log.SetOutput(os.Stderr)

	if *shouldReportVersion == true {
		fmt.Println(VERSION)
		return
	}
	if *commandLineCommand != "" {
		RunInCLI = true
		if *commandLineDebug == true {
			Logging = true
			Debug = true
		}
		switch *commandLineCommand {
		case "do-something":
			// ...
		default:
			fmt.Println("unknown command: " + *commandLineCommand)
		}
		return
	}

	rawRequest, err := chrome.Receive(os.Stdin)
	if err != nil {
		log.Fatal(err)
	}
	request := &Request{
		Logging:          false,
		Debug:            false,
		LogRotationCount: 7,
		LogRotationTime:  24,
	}
	if err := json.Unmarshal(rawRequest, request); err != nil {
		log.Fatal(err)
	}

	Logging = request.Logging
	Debug = request.Debug
	if Logging {
		logfileDir := os.ExpandEnv(`${temp}`)
		logRotationTime := time.Duration(request.LogRotationTime) * time.Hour
		logRotationCount := request.LogRotationCount
		maxAge := time.Duration(-1)
		// for debugging
		//logRotationTime = time.Duration(request.LogRotationTime) * time.Minute
		rotateLog, err := rotatelogs.New(filepath.Join(logfileDir, "com.clear_code.applicationtemplate_host.log.%Y%m%d%H%M.txt"),
			rotatelogs.WithMaxAge(maxAge),
			rotatelogs.WithRotationTime(logRotationTime),
			rotatelogs.WithRotationCount(logRotationCount),
		)
		if err != nil {
			log.Fatal(err)
		}
		defer rotateLog.Close()

		log.SetOutput(rotateLog)
		log.SetFlags(log.Ldate | log.Ltime)
		LogForDebug("logRotationCount:" + fmt.Sprint(logRotationCount))
		LogForDebug("logRotationTime:" + fmt.Sprint(logRotationTime))
	}

	LogForInfo("Command:" + request.Command)

	switch command := request.Command; command {
	case "do-something":
		// ...
	default: // just echo
		err = chrome.Post(rawRequest, os.Stdout)
		if err != nil {
			log.Fatal(err)
		}
	}
}
