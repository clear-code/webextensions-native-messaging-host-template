# webextensions-native-messaging-host-template

A template of Native Messaging Host for WebExtensions addons.
This contains resources to build Windows MSI.

## How to use

Put files in this project to a direcrtory named `native-messaging-host` under your WebExtensions project containing `manifest.json`.
